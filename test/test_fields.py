import pytest
from django import forms
from django_iambot import ValidChoiceField


@pytest.fixture
def form():
    class TheForm(forms.Form):
        aux_fld = forms.CharField(label="Name", required=False)
        fld = ValidChoiceField(
            help_text="What kind of *creature* you are not?",
            choices=(
                ("flying-saucer", "Flying saucer"),
                ("bot", "A very smart bot"),
                ("sapiens", "A homo sapiens"),
            ),
            valid="bot",
        )

    return TheForm


@pytest.mark.parametrize("val", ["flying-saucer", "sapiens", "other"])
def test_invalid_answer(form, val):
    assert not form({"fld": val}).is_valid()


def test_valid_answer(form):
    assert form({"fld": "bot"}).is_valid()


def test_has_empty_choice_by_default(form):
    choices = form.base_fields["fld"].choices
    assert choices[0][0] is None


def test_no_empty_choice():
    class form(forms.Form):
        fld = ValidChoiceField(
            valid="only", choices=(("only", "Dummy"),), null_choice=False
        )
    assert form.base_fields["fld"].choices[0][0] == "only"


def test_custom_empty_choice():
    class form(forms.Form):
        fld = ValidChoiceField(
            valid="some", choices=((None, "please pick"), ("some", "Value"))
        )
    assert form.base_fields["fld"].choices[0][1] == "please pick"
