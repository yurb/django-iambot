from django.forms import ChoiceField, ValidationError
from django.utils.translation import gettext_lazy as _

_DEFAULT_INVALID_MSG = _("Oops, you have failed the bot check! Try once more?")
_NULL_CHOICE = (None, "--------")


def has_none_choice(choices):
    return next((True for el in choices if el[0] is None), False)


class ValidChoiceField(ChoiceField):
    def __init__(
        self,
        valid,
        choices=list(),
        null_choice=True,
        msg=_DEFAULT_INVALID_MSG,
        *args,
        **kwargs
    ):
        _choices = (
            choices
            if not null_choice or has_none_choice(choices)
            else (_NULL_CHOICE, *choices)
        )
        super().__init__(choices=_choices, *args, **kwargs)
        self.msg = msg
        self.valid = valid

    def clean(self, value):
        if value != self.valid:
            raise ValidationError(self.msg)
