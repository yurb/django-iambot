"""A simple ChoiceField that requires a specific choice to validate.

Can be used as a very dumb (yet still effective) captcha solution on forms.
"""

__version__ = "0.1.2"

from .fields import ValidChoiceField
