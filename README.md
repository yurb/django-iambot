A simple `ChoiceField` subclass that requires a specific predefined
choice in order to validate.

Can be used as a very primitive (yet still somewhat effective)
honeypot-like anti-spam solution on forms.

Example use:

```python
from django import forms
from django.utils.translation import gettext_lazy as _
from django_iambot import ValidChoiceField


class FeedbackForm(forms.Form):
    # <your normal fields here>
    # then something like:

    iambot = ValidChoiceField(
        label=_("You aren't a bot, are you?"),
        help_text=_("Which of these you are definitely not?"),
        choices=(
            ("intelligent", _("An intelligent being")),
            ("living", _("A carbon-based form of life")),
            ("bot", _("A preprogrammed machine")),
        ),
        valid="bot",
        msg=_("Oops, you have failed the bot check! Try once more?")
    )
```

The form will only validate if the user selected the option specified in `valid`.

## Arguments

The `ValidationChoiceField` accepts the same arguments as `ChoiceField` and few additional ones:

- `valid` - the value that the user needs to pick in order for the field to validate.
- `msg` - overrides the predefined error message shown when the field doesn't validate.
- `null_choice` - if `True` (default) and no option with `None` value
  is provided in `choices`, then such option is prepended
  automatically to `choices` with `--------` as the displayed
  text. Set to `False` to exclude a `None` option from output.
